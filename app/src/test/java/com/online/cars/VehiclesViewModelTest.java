package com.online.cars;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;

import com.online.cars.pojo.Vehicle;
import com.online.cars.repository.VehicleRepository;
import com.online.cars.viewModel.VehiclesViewModel;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class VehiclesViewModelTest {

    @Rule
    public TestRule testRule = new InstantTaskExecutorRule();

    private VehiclesViewModel vehiclesViewModel;

    @Mock
    private VehicleRepository contactFormRepository;



        @Test
        public void testSortVehicles() {

            List<Vehicle> actual = Arrays.asList(new Vehicle(1,"b"),new Vehicle(2,"a") , new Vehicle(3,"1"));
            List<Vehicle> expected = Arrays.asList(new Vehicle(3,"1"),new Vehicle(2,"a") , new Vehicle(1,"b"));

            vehiclesViewModel.sortVehicles(actual);
            //check empty list
            assertThat(actual.isEmpty(), is(false));

            // Test equal.
            assertThat(actual, is(expected));

            //If List has this value?
            assertThat(actual, hasItems(new Vehicle(1,"b")));

            //Check List Size
            assertThat(actual.size(), is(3));


        }


}
