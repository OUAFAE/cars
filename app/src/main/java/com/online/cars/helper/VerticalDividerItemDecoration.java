package com.online.cars.helper;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.DimenRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class VerticalDividerItemDecoration extends RecyclerView.ItemDecoration {
    int margin;
    int marginTop;



    /**
     * Define the standard margin dimen
     *
     * @param context       to get resources
     * @param dpMarginDimen is to make sure you put dimen in res
     */
    public VerticalDividerItemDecoration(Context context, @DimenRes int dpMarginDimen) {
        this.margin = (int) context.getResources().getDimension(dpMarginDimen);
        this.marginTop = (int) context.getResources().getDimension(dpMarginDimen);
    }

    /**
     * Define the standard margin dimen with tab title
     *
     * @param context       to get resources
     * @param dpMarginDimen is to make sure you put dimen in res
     */
    public VerticalDividerItemDecoration(Context context, @DimenRes int dpMarginDimen, int marginTop, int marginBottom) {
        this.margin = (int) context.getResources().getDimension(dpMarginDimen);
        this.marginTop = marginTop;
        this.marginTop = marginBottom;
    }

    /**
     * Generate white space around each item. Rules:
     * - Everyone has start(left) margin
     * - Everyone has end(right) margin
     * - Everyone has bottom margin
     * - if it is the first item, it has top margin except in projects fragment
     * - All the margins has the same thickness
     *
     * @param outRect
     * @param view
     * @param parent
     * @param state
     */
    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildLayoutPosition(view);
        int viewType = parent.getAdapter().getItemViewType(position);
        if (position == 0) {
            outRect.top = marginTop;

        }
        outRect.bottom = margin;
        outRect.left = margin;
        outRect.right = margin;

    }





}

