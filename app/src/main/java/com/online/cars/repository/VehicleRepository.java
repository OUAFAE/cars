package com.online.cars.repository;

import android.arch.lifecycle.MutableLiveData;

import com.online.cars.network.ApiMediator;
import com.online.cars.pojo.Vehicle;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehicleRepository {

    @Inject
    public ApiMediator apiMediator;
    private MutableLiveData<Vehicle> vehicleSource = new MutableLiveData<>();
    private MutableLiveData<List<Vehicle>> vehiclesSource = new MutableLiveData<>();
    private MutableLiveData<Exception> error = new MutableLiveData<>();

    @Inject
    public VehicleRepository( ) {

    }

    public void loadVehicles() {
        apiMediator.getVehicles( new Callback<List<Vehicle>>() {
            @Override
            public void onResponse(Call<List<Vehicle>> call, Response<List<Vehicle>> response) {
                vehiclesSource.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<Vehicle>> call, Throwable t) {
                if (t instanceof Exception) {
                    Exception exception = (Exception) t;
                    error.setValue((Exception) t);
                }
            }
        });
    }



    public MutableLiveData<Vehicle> getVehicleSource() {
        return vehicleSource;
    }


    public MutableLiveData<List<Vehicle>> getVehiclesSource() {
        return vehiclesSource;
    }

    public MutableLiveData<Exception> getError() {
        return error;
    }

}
