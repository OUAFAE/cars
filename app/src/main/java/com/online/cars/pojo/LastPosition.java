package com.online.cars.pojo;

import com.google.gson.annotations.SerializedName;

public class LastPosition {

    @SerializedName("timestamp")
    private String timestamp;
    @SerializedName("lat")
    private float lat;
    @SerializedName("lng")
    private float lng;


    public LastPosition(String timestamp, float lat, float lng) {
        this.timestamp = timestamp;
        this.lat = lat;
        this.lng = lng;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(long lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }
}
