package com.online.cars.pojo;

import com.google.gson.annotations.SerializedName;

public class Vehicle {

    @SerializedName("id")
    private int id;
    @SerializedName("licensePlate")
    private String licensePlate;
    @SerializedName("brand")
    private String brand;
    @SerializedName("model")
    private String model;
    @SerializedName("nickname")
    private String nickname;
    @SerializedName("lastPosition")
    private LastPosition lastPosition;


    public Vehicle(int id, String licensePlate) {
        this.id = id;
        this.licensePlate = licensePlate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getNickname() {
        return nickname == null ? "" : nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public LastPosition getLastPosition() {
        return lastPosition;
    }

    public void setLastPosition(LastPosition lastPosition) {
        this.lastPosition = lastPosition;
    }
}
