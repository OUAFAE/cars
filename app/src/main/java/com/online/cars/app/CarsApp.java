package com.online.cars.app;

import android.app.Activity;
import android.support.multidex.MultiDexApplication;

import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.online.cars.di.AppComponent;
import com.online.cars.di.DaggerAppComponent;

import java.util.Locale;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import okhttp3.OkHttpClient;
import timber.log.Timber;

public class CarsApp extends MultiDexApplication implements HasActivityInjector {

    //A single Okhttp object for retrofit
    private static OkHttpClient client;
    private static AppComponent appComponent;
    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        // Normal app init code...
        //if (BuildConfig.DEBUG) {
        Timber.plant(new Timber.DebugTree());
        Stetho.initializeWithDefaults(this);
        appComponent = DaggerAppComponent
                .builder()
                .application(this)
                .build();

        appComponent.inject(this);


    }

    public static OkHttpClient getOkhttpClient() {
        if (client == null) {
            client = new OkHttpClient.Builder()
                    .addNetworkInterceptor(new StethoInterceptor())
                    .build();
        }
        return client;
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    /**
     * The device's locale
     * It will return like 'en'
     *
     * @return 1 locale
     */
    public static Locale getLocale() {
        return Locale.getDefault();
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityDispatchingAndroidInjector;
    }
}
