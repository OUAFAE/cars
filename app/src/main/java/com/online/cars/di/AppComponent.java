package com.online.cars.di;

import android.app.Application;
import android.support.v4.view.KeyEventDispatcher;

import com.online.cars.app.CarsApp;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {ApiServiceModule.class, AndroidSupportInjectionModule.class,
        ActivityBuilderModule.class, FragmentBuilderModule.class})
public interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }

    void inject(CarsApp app);
}

