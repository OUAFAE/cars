package com.online.cars.di;

import com.online.cars.view.VehiclesFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuilderModule {

    @ContributesAndroidInjector()
    abstract VehiclesFragment contributeVehiclesFragment();
}