package com.online.cars.di;

import com.online.cars.scope.ActivityScope;
import com.online.cars.view.MainActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilderModule {

    @ContributesAndroidInjector()
    @ActivityScope
    abstract MainActivity contributeMainActivity();
}