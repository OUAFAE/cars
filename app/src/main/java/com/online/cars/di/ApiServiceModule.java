package com.online.cars.di;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.online.cars.BuildConfig;
import com.online.cars.app.CarsApp;
import com.online.cars.network.ApiInterface;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

@Module
public class ApiServiceModule {
    private static final String BASE_URL = BuildConfig.BASE_URL;

    @Provides
    @Singleton
    ApiInterface provideApiInterface(Retrofit retrofit) {
        return retrofit.create(ApiInterface.class);
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit() {
        Retrofit retrofit = null;
        try {
            Gson gson = new GsonBuilder()
                    .serializeNulls()
                    .create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(CarsApp.getOkhttpClient())
                    .build();
        } catch (NullPointerException e) {
            Timber.e(new Throwable("error message ", e)); //NON-NLS
        }
        return retrofit;
    }

    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }
}
