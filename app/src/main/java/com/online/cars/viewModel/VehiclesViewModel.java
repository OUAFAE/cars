package com.online.cars.viewModel;

import android.arch.lifecycle.MutableLiveData;

import com.online.cars.app.CarsApp;
import com.online.cars.pojo.Vehicle;
import com.online.cars.repository.VehicleRepository;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

public class VehiclesViewModel {

    private VehicleRepository repository;

    @Inject
    public VehiclesViewModel(VehicleRepository repository) {
        this.repository = repository;
    }

    public MutableLiveData<List<Vehicle>> getVehicles() {
        //Java 8
        //repository.getVehiclesSource().getValue().sort(Comparator.comparing(Vehicle::getLicensePlate));
        return repository.getVehiclesSource();
    }

    public void sortVehicles( List<Vehicle> sortedVehicles){
        if (sortedVehicles != null && sortedVehicles.size() > 0) {
            Collections.sort(sortedVehicles, new Comparator<Vehicle>() {
                Collator collator = Collator.getInstance(CarsApp.getLocale());
                @Override
                public int compare(final Vehicle car1, final Vehicle car2) {
                    return collator.compare(car1.getLicensePlate(), car2.getLicensePlate());
                }
            });
        }
    }

    public MutableLiveData<Exception> getError() {

        return repository.getError();
    }
    public void loadVehicles() {
        repository.loadVehicles();
    }

}
