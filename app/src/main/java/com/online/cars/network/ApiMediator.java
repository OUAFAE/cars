package com.online.cars.network;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.online.cars.pojo.Vehicle;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Callback;

public class ApiMediator {

    private static final String TAG = "ApiMediator"; //NON-NLS
    public  ApiInterface apiService;
    private Context context;

    public ApiMediator(Fragment fragment) {
        apiService = new ApiClient().getService(ApiInterface.class);
    }

    @Inject
    public ApiMediator(ApiInterface apiMediator) {
        apiService = apiMediator;
    }

    /**
     * Get users list.
     *
     * @param callback the callback
     */
    public void getVehicles(Callback<List<Vehicle>> callback) {

        apiService.getVehicles().enqueue(callback);
    }


}
