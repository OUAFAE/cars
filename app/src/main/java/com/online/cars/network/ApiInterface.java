package com.online.cars.network;

import com.online.cars.pojo.Vehicle;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiInterface {

    @GET("vehicles.json")
    Call<List<Vehicle>> getVehicles();



}
