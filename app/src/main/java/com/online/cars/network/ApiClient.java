package com.online.cars.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.online.cars.BuildConfig;
import com.online.cars.app.CarsApp;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

public class ApiClient {

    private static final String TAG = "ApiClient"; //NON-NLS
    private static final String BASE_URL = BuildConfig.BASE_URL ;

    private Retrofit retrofit;

    public  Retrofit getRetrofit() {
        return retrofit;
    }

    public void setRetrofit(Retrofit retrofit) {
        this.retrofit = retrofit;
    }


    /**
     * Constructor with Okhttp
     */
    public ApiClient() {
        try {
            Gson gson = new GsonBuilder()
                    .serializeNulls()
                    .create();
            if (retrofit == null) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .client(CarsApp.getOkhttpClient())
                        .build();
            }
        } catch (NullPointerException e) {
            Timber.tag(TAG).e(new Throwable("if you are reading from Fabric keep the catch, otherwise this is no longer useful", e)); //NON-NLS
        }
    }



    public <T> T getService(Class<T> serviceClass) {
        return retrofit.create(serviceClass);
    }

}
