package com.online.cars.view;

import com.online.cars.pojo.Vehicle;

public interface ListItemClickListener {
    /**
     * Callback to observer of which item has been clicked
     *
     * @param position of the item (if that is useful)
     * @param item     itself
     */
    void onItemSelect(int position, Vehicle item);


}
