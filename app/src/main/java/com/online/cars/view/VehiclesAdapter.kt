package com.online.cars.view


import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView

import com.online.cars.R
import com.online.cars.pojo.Vehicle
import java.util.Locale
import java.util.function.ToDoubleBiFunction

class VehiclesAdapter
/**
 * Default constructor
 *
 * @param list
 */
(private val vehicles: MutableList<Vehicle>?) : RecyclerView.Adapter<VehiclesAdapter.VehiclesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VehiclesViewHolder {

        return VehiclesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_car, parent, false))
    }

    override fun onBindViewHolder(holder: VehiclesViewHolder, position: Int) {
        if (vehicles != null) {
            val thisCar = vehicles[holder.adapterPosition]
            if (holder.license != null) {
                holder.license!!.text = thisCar.licensePlate
            }

            if (holder.model != null && thisCar.model != null) {
                holder.model!!.text = thisCar.model
            }

            if (holder.brand != null && thisCar.model != null) {
                holder.brand!!.text = thisCar.brand
            }

            if (holder.buttonMap != null && thisCar.lastPosition != null) {
                // holder.buttonMap.setText(thisCar.getLastPosition());
                holder.buttonMap!!.setOnClickListener { v ->
                    seeMap(v.context, thisCar.lastPosition.lat.toDouble(), thisCar.lastPosition.lng.toDouble(),
                            thisCar.nickname)
                }
            }
            holder.cardView.setOnClickListener {
                //TODO open details of car
                //listItemClickListener.onItemSelect(holder.getAdapterPosition(), thisCar);
            }

        }
    }


    /**
     * Open Google Maps with your location with a marker on it
     *
     * @param latitude  of the car
     * @param longitude of the car
     * @param name      to be display on google map as the title of the place
     */
    private fun seeMap(context: Context, latitude: Double, longitude: Double, name: String) {
        val uriString = String.format(Locale.ENGLISH, "geo:%f,%f?q=%f,%f(%s)", latitude, longitude, latitude, longitude, name)  //NON-NLS
        val gmmIntentUri = Uri.parse(uriString)
        var mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")                                       //NON-NLS
        if (mapIntent.resolveActivity(context.packageManager) != null) {
            context.startActivity(mapIntent)
        } else {    //no google map -> ask the whole system for whoever can open "geo:"
            mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            if (mapIntent.resolveActivity(context.packageManager) != null) {
                context.startActivity(mapIntent)
            }
        }
    }

    /**
     * Call this when you want to clear the whole list (e.g. refresh, apply new filter, etc)
     */
    fun clearAllItems() {
        val size = itemCount
        vehicles!!.clear()
        notifyItemRangeRemoved(0, size)
    }

    override fun getItemCount(): Int {
        return vehicles?.size ?: 0
    }

    /**
     * see item_car.xml
     */
     class VehiclesViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var license: TextView? = null
        var model: TextView? = null
        var brand: TextView? = null
        var cardView: CardView
        var buttonMap: ImageButton? = null
        var divider: View? = null

        init {
            license = v.findViewById(R.id.license)
            model = v.findViewById(R.id.model)
            brand = v.findViewById(R.id.brand)
            cardView = v.findViewById(R.id.card_view)
            buttonMap = v.findViewById(R.id.map)

        }
    }
}
