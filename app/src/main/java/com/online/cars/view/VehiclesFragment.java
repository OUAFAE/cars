package com.online.cars.view;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.online.cars.R;
import com.online.cars.helper.VerticalDividerItemDecoration;
import com.online.cars.pojo.Vehicle;
import com.online.cars.repository.VehicleRepository;
import com.online.cars.viewModel.VehiclesViewModel;
import java.util.List;
import javax.inject.Inject;
import dagger.android.support.AndroidSupportInjection;
import timber.log.Timber;

public class VehiclesFragment extends Fragment {

    private VehiclesAdapter adapter;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Inject
    public VehiclesViewModel vehiclesViewModel;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(getContentViewLayout(), container, false);

    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recycler_view);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        if (getItemDecoration() != null) {
            recyclerView.addItemDecoration(getItemDecoration());
        }
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    if (adapter != null) {
                        Timber.d("showError clearItems in swipeRefreshLayout");
                        adapter.clearAllItems();
                        loadVehicles();
                    }

                }
            });
        }
        loadVehicles();
        observeSearchSuggestions();
    }


    protected int getContentViewLayout() {
        return R.layout.fragment_main;
    }


    /**
     * This defines the divider between each items. Override this to use your custom decoration
     *
     * @return
     */
    protected RecyclerView.ItemDecoration getItemDecoration() {
        return new VerticalDividerItemDecoration(getContext(), R.dimen.standard_left_right_margin);
    }

    /**
     * Function to load vehicles and save result
     */
    public void loadVehicles() {
        vehiclesViewModel.loadVehicles();
    }


    /**
     * Call this when error to dismiss
     * 1) disable the refreshing spinner
     */
    public void dismissSwipe() {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }
    /**
     * Expose the LiveData users query so the UI can observe it.
     */
    public void observeSearchSuggestions() {
        vehiclesViewModel.getVehicles().observe(this, new Observer<List<Vehicle>>() {
            @Override
            public void onChanged(@Nullable List<Vehicle> cars) {
                if (recyclerView != null && cars != null) {
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                    vehiclesViewModel.sortVehicles(cars);
                    adapter = new VehiclesAdapter(cars);
                    recyclerView.setAdapter(adapter);
                    dismissSwipe();
                }
            }
        });
    }

    /**
     * here we Update the view of vehicles when an error is received
     */
    private void observeVehiclesErrorViewModel() {
        vehiclesViewModel.getError().observe(VehiclesFragment.this, new Observer<Exception>() {
            @Override
            public void onChanged(@Nullable Exception exception) {
                dismissSwipe();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }


}
